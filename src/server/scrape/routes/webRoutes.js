import UI_PARADE_MODEL from '../model/webpages_models/Ui_parade_model';
import UI_MOVEMENT_MODEL from '../model/webpages_models/Ui_movement_model';

export const WEB_CONFIG = {
    uiparade: {
      code: 'UIP',
      name: 'UI Parade',
      domine_name: 'uiparade',
      url: 'http://www.uiparade.com',
      parser: UI_PARADE_MODEL.jsonParse,
    },
    uimovement: {
      code: 'UIM',
      name: 'UI Movement',
      domine_name: 'uimovement',
      url: 'https://uimovement.com',
      parser: UI_MOVEMENT_MODEL.jsonParse,
    }
};

export const URLS_BY_CATEGORY = {
  base: [
    WEB_CONFIG.uiparade.url,
    WEB_CONFIG.uimovement.url
  ],
  categories: {
    filter: [
      WEB_CONFIG.uimovement.url + '/tag/filter/'
    ],
    button: [
      WEB_CONFIG.uiparade.url + '/skill-type/buttons/',
      WEB_CONFIG.uimovement.url + '/tag/button/'
    ]
  }
};
