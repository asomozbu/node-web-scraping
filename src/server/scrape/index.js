const rp = require('request-promise');
const cheerio = require('cheerio');
import * as R from 'ramda';
import {WEB_CONFIG, URLS_BY_CATEGORY} from './routes/webRoutes';
const urlModel = require('./routes/webRoutes');

const finished = function(num) { return after(num); };

const getUrl = function(categoryUrl) {
  return !categoryUrl ?
    URLS_BY_CATEGORY.base :
    URLS_BY_CATEGORY.categories[categoryUrl];
};

const parseWebCode = obj => obj.domine_name;

const getWebCode = url => R.map(parseWebCode, WEB_CONFIG);

function runScraping(options, url) {
  const webCode = Object.values(getWebCode(url)).filter(code => url.includes(code));

  return rp(options)
      .then(function ($) {
          return {[webCode[0]]: $};
      })
      .catch(function (err) {
          console.log(err);
      });
}

async function scrapeData(url) {
  const scrapeUrl = getUrl(url);

  const HTML = await Promise.all(
    scrapeUrl.map( async function(url) {
      const OPTIONS = {
        uri: url,
        transform: function (body) {
          return cheerio.load(body);
        }
      };

      const response = await runScraping(OPTIONS, url);
      return response;
    }));
  return HTML;
}

export default scrapeData;
