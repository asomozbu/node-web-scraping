
import {WEB_CONFIG} from '../../routes/webRoutes';
import BaseEntity from '../baseEntity';

const ENTITY_NAME = 'uimovement';
const RESOURCE_WRAPPER_CLASS = 'resources-wrapper';
const GIF_WRAPPER_CLASS = 'video-thumbnail-link';
const GIF_WRAPPER_PROPERTY = 'data-gif-animated-url';
const CREATOR_WRAPPER_CLASS = 'user-resource-user-deets';
const CREATOR_LINK = 'a';
const CREATOR_LINK_PROPERTY = 'href';

const TAG_MARKUP = {
  wrapper: RESOURCE_WRAPPER_CLASS,
  gif: {
    wrapper: GIF_WRAPPER_CLASS,
    property: GIF_WRAPPER_PROPERTY,
  },
  creator: {
    wrapper: CREATOR_WRAPPER_CLASS,
    link: CREATOR_LINK,
    property: CREATOR_LINK_PROPERTY,
  }
};

class Ui_movement_entity extends BaseEntity {
  constructor() {
    super();
  }

  getCratorName($, e, creatorTag) {
      return $(e).find('.' + creatorTag.wrapper + ' ' + creatorTag.link + ':nth-child(2)').text();
  }

  getCratorLink($, e, creatorTag) {
    return $(e).find('.' + creatorTag.wrapper + ' ' + creatorTag.link + ':nth-child(2)').attr(creatorTag.property);
  }

  jsonParse = (webScraped) => {
    const baseUrl = WEB_CONFIG[ENTITY_NAME].url;
    return this.cheerioParser(webScraped[ENTITY_NAME], TAG_MARKUP, baseUrl);
  }
}

export default new Ui_movement_entity();
