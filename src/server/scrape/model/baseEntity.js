const cheerio = require('cheerio');

class BaseEntity {
  constructor() {}

  cheerioParser(data, tagMarkup, baseUrl){
      const $ = cheerio.load(data.html());
      let scrapedInfo = [];
      
      $('.' + tagMarkup.wrapper).each((i, element) => {
        const gifMediaPath = baseUrl + this.getGifElement($, element, tagMarkup.gif);
        const creatorName = this.getCratorName($, element, tagMarkup.creator);
        const creatorLink = this.getCratorLink($, element, tagMarkup.creator);
        const scrapedInfoDetail = {
          gifPath: gifMediaPath,
          creatorName,
          creatorLink,
        };
        scrapedInfo = [...scrapedInfo, scrapedInfoDetail];
      });

      return scrapedInfo;
  }

  getCratorName($, e, creatorTag) {
      return $(e).find('.' + creatorTag.wrapper + ' ' + creatorTag.link).text();
  }

  getCratorLink($, e, cratorTag) {
    return $(e).find('.' + creatorTag.wrapper + ' ' + creatorTag.link).attr(creatorTag.property);
  }

  getGifElement($, e, gifTag) {
    return $(e).find('.' + gifTag.wrapper).attr(gifTag.property);
  }

}

export default BaseEntity;
