import scrapeService from '../controllers/controller';

export default function(app) {

  // todoList Routes
  app.route('/api')
    .get(scrapeService);

    app.route('/api/:category')
      .get(scrapeService);
}
