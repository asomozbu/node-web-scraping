
import * as R from 'ramda';
import scrapeService from '../../scrape/index';
import {WEB_CONFIG} from '../../scrape/routes/webRoutes';

const parsedData = (webScraped) => {
  const objKey = Object.keys(webScraped).shift();
  return WEB_CONFIG[objKey].parser(webScraped);
};

const parseScrapedData = function($) {
  R.map(parsedData, $);
};

export default function(req, res) {
  const reqParams = req.params && req.params.category ? req.params.category : false;
  scrapeService(reqParams).then(function($){
    parseScrapedData($);
  });
}
