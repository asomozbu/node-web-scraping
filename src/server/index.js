var express = require('express'),
  app = express(),
  port = process.env.PORT || 8080,
  bodyParser = require('body-parser');

import routes from './api/routes/routes'; //importing route

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

routes(app); //register the route

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
