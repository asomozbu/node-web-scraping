const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const NodemonPlugin = require( 'nodemon-webpack-plugin' ); // Ding
const outputDirectory = "dist/server";

module.exports = {
    entry: ["babel-polyfill" ,"./src/server/index.js"],
    output: {
        path: path.join(__dirname, outputDirectory),
        filename: 'server.js',
    },
    target: 'node',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["ts-loader", "babel-loader"]
            },
        ]
    },
    plugins: [
        new NodemonPlugin(), // Dong
    ],
};
